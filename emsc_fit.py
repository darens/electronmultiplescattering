#!/usr/bin/env python
"""
    emsc_fit.py
    ~~~~~~~~~~~
    
    fit a electron multiple scattering ascii file to Gaussian

    :copyright: (c) 2016 by Daren Sawkey.
    :license: BSD, see LICENSE for more details.
"""
# input is the output data of electronScattering2 example.
# first column is the inside radius of ring
# third column is electron fluence for that ring
# the ring spacing is 1 mm

import os,sys
import shutil
import numpy as np
#from matplotlib import *
import random
import argparse
from scipy import optimize
import math


def emsc_fit(fn, outputfile='width.EE.dat'):
    print 'Analyzing', fn
    with open(fn, 'r') as f:
        ll = f.readlines()
    data_x = []
    data_y = []
  
    for l in ll:
        if l[0] != '#' and len(l) > 3:
            # take center of bin (add 0.5)
            data_x.append(float(l.split()[0])+0.5)
            ## electron fluence is the third column
            data_y.append(float(l.split()[2]))
  
    data_x = np.array(data_x)
    data_y = np.array(data_y)
    error_y = []
    for i in range(len(data_y)):
        # recall ring spacing is 1 mm
        area = 2 * np.pi * data_x[i]
        ## uncertainty (error) is sqrt(N)/A = sqrt(fluency)/sqrt(A)
        error = math.sqrt(data_y[i])/math.sqrt(area)
        error_y.append(error)
    error_y = np.array(error_y)

    #for i,x in enumerate(data_x):
    #  print x,data_y[i],error_y[i]
  
    fitfunc = lambda p,x: p[0]*np.exp(-x*x/p[1])
    errfunc = lambda p,x,y,err: (fitfunc(p,x)-y)/err

    p0 = [5000.,20.]
  
    # what are the initial guesses:
    p0[0] = data_y[0]
    tmpp1 = 1.
    for i in range(len(data_x)):
        if data_y[i]/data_y[0] < 0.37:
            p0[1] = data_x[i]*data_x[i]
            break
  
    # what is the fit range? only fit to min of 170 and 1/e point
    # 170 is used because of support ring
 
    fit_range = 0
    for i in range(len(data_x)):
        if data_y[i]/data_y[0] > 0.37 and data_x[i] < 170:
            fit_range = i

    try:
        p1, cov_x, infodict, mesg, ier = optimize.leastsq(
            errfunc,
            p0[:],
            args=(data_x[:fit_range], data_y[:fit_range], error_y[:fit_range]),
            full_output=True
        )
        
        angle = 360./2./math.pi*math.atan(math.sqrt(p1[1])/1182.)
        chisq =  sum(infodict["fvec"]*infodict["fvec"])
        dof = fit_range - len(p1)
        p1error = np.sqrt(cov_x[1,1])*np.sqrt(chisq/dof)
        angleerror = angle*p1error/p1[1]

    except:
        print 'Error fitting data file', fn
        p1 = [0, 0]
        angle = 0.
        chisq = 0
        dof = 1
        angleerror = 0
  
    #print p1
    #print cov_x
    #print infodict
    #print mesg
    #print ier
    #print success
  

    outstring = (
        '{0:40s}{1:9.4f} +- {2:9.4f} {3:9.4f}  {4:9.4f} {5:9.4f} {6:9.4f}'
        .format(fn, angle, angleerror, chisq, chisq/dof, p1[0], p1[1]))
    print outstring

    # TODO: add a header to the file
    energy = '13'
    if '_20_' in fn:
        energy = '20'
    outfilename = outputfile.replace('EE',energy)
    f = open(outfilename, 'a')
    f.write(outstring + '\n')
    f.close()

    #plt.figure()
    #plt.errorbar(data_x,data_y,yerr=error_y,fmt='ro')
    #plt.plot(data_x,fitfunc(p1,data_x),'b-')
    ###plt.show()
    #plt.savefig(fn+'.pdf',bbox_inches=0)


### main file

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='fit emsc result to Gaussian')
    parser = argparse.ArgumentParser(description=__doc__,
                formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('filename')
    args = parser.parse_args()

    import matplotlib.pyplot as plt
    
    if args.filename == 'all':
        ls2 = os.listdir('.')
        ls1 = []
        for fn in ls2:
            if (fn[-5:] == 'ascii' and os.stat(fn)[6] > 500
                    and fn[:5] != 'chisq'):
                ls1.append(fn)
        for fn in ls1:
            emsc_fit(fn)
    else:
        emsc_fit(args.filename)
